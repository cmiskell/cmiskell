## Background

After working/playing with computers for years as a child + teenager, I started my career as a Web Developer in the late 1990s; after 5 years of that with a small side-dish of systems administration, I moved full time to sysadmin, varying in focus from High Performance Compute (on what would now be a laughably small scale), to Corporate IT, to general Open Source consulting and support, to "DevOps", before landing at GitLab in 2019.

## What I work on

Anything FOSS.  Windows literally gives me a headache (when I previously had to occasionally deal with Windows in more than a passing manner I would usually have a headache at the end of the day); it simply doesn't work how I want computers to work, and it hurts.  Otherwise, I'll give anything a try; if I can start with installing it and then work my way down the various layers into the source code until I've made it work or found why it can't work, I'm happy.

I particularly enjoy weird/strange/complicated technical issues, so if you're stuck on something strange and have no further leads to follow, or are out of time on something urgent, I'll be delighted to help (subject to availability).  I have a dream of one day opening a consulting business focused solely on debugging the weirdest edge case problems, with a sliding scale of fees that gets cheaper the more interesting/weird the problem is and the more I can publish about the investigation, whereas if I can find the answer on the first page of my first Google search, the fee is astronomical.  I may need to win the lottery first to fund it, but it'd be lots of fun.

Specifically at GitLab I'm in the [Dedicated Group](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/) where we build and operate GitLab's [Single-Tenant SaaS](https://about.gitlab.com/dedicated/) offering; as a Principal SRE there, my job is to provide support, guidance, and leadership on technical matters across the entire group.  I sometimes even get to write my own code (/s).

## Organisation 

I use a local [Kanban board](https://greggigon.com/my-personal-kanban-2-0/) to manage my immediate day-to-day and medium-term work.  Todos and e-mails are, after initial filtering, turned into issues/cards on that board for Today; others (including random ideas that sprung to mind) may be in various 'future' categories from Tomorrow to Maybe Never

I strive very hard for Inbox Zero and GitLab Todo Zero.  I consider unblocking colleagues one of my higher priorities, so I try very hard to respond to Todos that are blocking within a working day.  If I haven't responded in some manner within a couple of working days, I've likely lost/missed it and may need another prompt (and will be happy to receive it).

I subscribe to many relevant projects/groups in GitLab to maintain awareness of what's going on, and as a result I get a lot of e-mails that I need to scan (and usually delete) quickly.  Therefore, please @-mention me if you need a response or action, or think it particularly important that I'm aware of something, even if it's a response to a thread I'm already involved in.  Closing TODOs is quick and easy, and I'd rather do that a dozen times a day than miss important information or requests.

## Communications

I prefer direct and unsubtle (but still kind) communication, and do not like having to guess what was intended, so I will generally assume the most obvious interpretation.  If you want me to know something, tell me straight.  In the other direction I aim to be similarly straightforward and precise in my word choice and phrasing, tempered by kindness.  I both love and hate the English language; it is an awkward filthy language full of exceptions and weirdness, but there is a word, phrase, or idiom for everything, even if it was stolen shamelessly from another language 300 years ago and thoroughly mangled in pronunciation and meaning since.

I welcome constructive feedback on how to do anything better; if I have made a mistake I prefer to know it so I can try not to make it again.  In particular, I strive never to be rude or obnoxious (although I may fail on hopefully rare occasions), so if I am it is probably unintentional, and I ask that you tell me directly so that I can correct my future behavior.

If I'm at work (at my desk in my dedicated office), I tend to respond fairly quickly to Slack unless I'm deep into something technical.  If I'm not at work (and not on-call), I occasionally open Slack on mobile but that's a bad habit and I am trying not to.  I do not have Mobile notifications enabled.  I do not have any access to any other GitLab accounts (e-mail, gitlab.com, etc) outside my work laptop, which stays at my desk 99.9% of the time, and I intend to keep it that way.

## Limitations

I speak fast sometimes, particularly when excited.  If I'm going too fast (particularly with my NZ accent), please feel free to ask me to slow down and/or repeat myself.

I sometimes write too many words when fewer would suffice; I'm getting better, but can still be far too wordy at times (even after the 3rd edit).

I can sometimes get locked into an indecision spiral from too many subtly different or conflicting options, but usually I am aware enough to ask for an external opinion and a little nudge will unstick me.  If you think you see it happening and can unwedge me, please do.

## Culture Map

In case it's useful when interacting with me, here's where I think I sit on the dimensions described by Erin Meyer in [The Culture Map](https://erinmeyer.com/books/the-culture-map/), which I highly recommend for anyone working across global cultures:

* Communication: I tend to (and prefer) low-context communications. I can sometimes read high-context nuance, but I'm not always great at it.
* Evaluating: I tend to the middle when giving negative feedback (couched carefully, but not so subtle it can be missed), although I prefer more direct for *receiving* negative feedback.  And yes I am *fully* aware of the subtle hypocrisy of this stance.
* Persuading: Fairly strongly to the principles-first side (why) with the application side helping to illuminate the why.
* Leading: Mostly egalitarian, with a hint of *earned* hierarchy.
* Deciding: Unclear/mixed; both have benefits depending on the situation, with consensus being necessary in some cases, and leader/DRI being necessary in others.
* Trusting: Generally cognitive/task based; relationship-based adds to that but takes time for me to build and I rely on both to generate a full trust picture of the other person.
* Disagreeing: More to the confrontational side.  I am happy to have a good logical discussion about anyone's ideas, but it can still be polite so "confrontational" is not really the word I'd like to use.
* Scheduling: Strongly linear when planning and interacting with others (meetings will be On Time, so help me...); in my alone time I multi-task heavily, because I find it necessary to do so.

## Miscellaneous facts

* I speak fluent GIF and emoji, and will often communicate reactions with those rather than actual words (which somewhat reflects my in-person communication style, which involves waving my hands about vigorously).
* I live in New Zealand so my timezone is NZST/NZDT (GMT+12 or GMT+13), and my usual core work hours are 08:00 to 16:30.  I can potentially be available from 07:30 with sufficient prior warning, and can be coherent up until around 21:00.  Exceptions can be made when necessary, but I pay the price afterwards.  Or: I'm good in the morning, and not so good at night.
* My pronouns are He/Him
* When I'm not working I:
  * Play Factorio; I used to play it far too much, now I'm past that and can usually only play it for maybe an hour at a time before I get twitchy and have to stop.  This is good for me, particularly for getting *other* things done
  * Make music; long-time piano player, self-taught bass guitar, and now learning the Double Bass since early 2021.
  * Bake treats, preferably when I can give it away, otherwise I eat far too much of it myself.
